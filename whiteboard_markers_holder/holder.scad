width=150;
depth=50;
height=50;
thickness=2;

gap=1;

angle=20;

//base
cube([width,depth,thickness]);

//left
cube([thickness,depth,height]);

//right
translate([width,0,0])
    cube([thickness,depth,height]);

//center
translate([gap,0,0])
    cube([thickness/2,depth,height*0.9]);

//front
translate([gap,0,0])
    rotate([angle,0,0])
        cube([width-gap+thickness,thickness,height*0.6]);
        
hull(){
    //center_hull_helper
    translate([gap,0,0])
        cube([thickness/2,1,height*0.9]);
    //front_left_hull_helper
    translate([gap,0,0])
        rotate([angle,0,0])
            cube([1,thickness,height*0.6]);
}

hull(){
    //right_hull_helper
    translate([width,0,0])
        cube([thickness,1,height]);
    //front_right_hull_helper
    translate([width,0,0])
        rotate([angle,0,0])
            cube([2,thickness,height*0.6]);
}

hull(){
    //back
    translate([0,depth,0])
        cube([width,thickness,height]);

    //attachment
    translate([width,depth,0])
    cube([32,thickness,50]);
}