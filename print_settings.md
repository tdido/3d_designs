# PLA
    - smooth sheet
# TenaFlex
    - prints:
        - Jeep tyres
        - cradle raisers
    - smooth sheet
    - extruder 240
    - bed 70
    - 50% speed
    - smooth sheet
# FilaFlex 40
    - prints:
        - Jeep tyres
    - textured/satin sheet
    - FilaFlex 40 settings
    - 70% speed
# PETG
    - prints:
        - window stops
    - textured sheet
    - Prusament PETG settings
