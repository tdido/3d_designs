include <Round-Anything/polyround.scad>

$fn=100;

width=29;
depth=10.5;
height=1;

module cyl(h,r){
    rotate([0,70,0])
        translate([5,depth/2,0])
            cylinder(h=h,r=r); 
}

module top(){
    translate([depth/2,0,0]){
        cube([width-depth,depth,height]);
        translate([0,depth/2,0])
            cylinder(h=height,r=depth/2);
        translate([width-depth,depth/2,0])
            cylinder(h=height,r=depth/2);
    }
    cyl(17,4);
}

module spring(){
    radiiPoints=[
        [4.5,0,0.5],
        [-6,10,0],
        [2,10,0],
        [4.5,3,1],
        [9,12,0],
        [13,12,0],
        [13,11,0],
        [11,11,0],
        [9.5,8,0],
        [12,8,0]
    ];
    linear_extrude(5.5)
        polygon(polyRound(radiiPoints,30));
}

difference(){
    hull(){
        difference(){
            top();
            translate([0,0,-10])
                cube([width,depth,10]);
        }
    }
    cyl(30,4);
    translate([20,2.25,0])
        cube([17,6,2]);
}
 
difference(){
    union(){
        cyl(17,4);
        rotate([90,0,0])
            translate([16.45,-11,-8])
                spring();
    }
    cyl(18,2.5);
    translate([0,1,-2.5])
        rotate([0,-20,0])
            cube([3,8,1.5]);
}



