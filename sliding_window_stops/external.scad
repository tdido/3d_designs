$fn=300;

length=150;
width=10;
height=18; //outside
//height=14.5; //inside

lock_height=8.6; //outside
//lock_height=5; //inside

module interior(l,w,h){
    minkowski(){
        translate([0,1,0])
            rotate([0,90,0])
                cylinder(h=l,r=1);
        translate([0,0,1])
            cube([l,w,h]);
    }
}

difference(){
    interior(length,width+4,height+2);
    translate([0,3,0])
        interior(length,width-2,height-2);
    translate([0,3,0])
        cube([length*2,width+2,2]);
}

translate([0,width+1,0])
    cube([length*2,4,lock_height]);