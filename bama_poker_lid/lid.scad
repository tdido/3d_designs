include <Round-Anything/polyround.scad>

points = [
    [0,0,0],
    [125,0,0],
    [123,52.5,100],
    [120,72.5,100],
    [115,94.5,100],
    [110,105.5,0],
    [100,107.5,10],
    [86,105.5,20],
    [78,102.5,10],
    [65,97.5,10],
    [50,92,200],
    [16,87,100],
    [0,87.5,0]
];

mirroredPoints=mirrorPoints(points,0,[1,0]);

module axis(){
    l=183;
    translate([3,l/2,3])
        rotate([90,90,0],$fn=30)
            cylinder(l,2,2);
}

module tab(){
    difference(){
        cylinder(h=2,r=65);
        translate([-80,-65,0])
            cube([130,130,2]);
    }
}

module axisgap(){
    l=175;
    translate([0,l/2,0])
        rotate([90,0,0])
            color("black")
                cube([8,4,l]);
}

module lid(){
    difference(){
        union(){
            //main shape
            extrudeWithRadius(6,1,0.5,30)
                polygon(polyRound(mirroredPoints,20));
            //protrusion
            scale([0.90,0.98,1])
            translate([10,0,3])
                extrudeWithRadius(6,1,0.5,30)
                    polygon(polyRound(mirroredPoints,20));
        }
        //hollow underside
        scale([0.95,0.95,0.85])
            translate([5,3,0])
                extrudeWithRadius(6,1,0.5,30)
                    polygon(polyRound(mirroredPoints,20));
        axisgap();
        //indentation for binding the halves when printing in two parts, not needed otherwise
        //translate([10,-30,4])
        //    cube([90,60,3]);

    }
}

lid();
axis();

$fn=100;
translate([65,0,7])
    color("red")
        tab();

// flat piece to join halves when printing in two parts
// cube([90,60,2]);
