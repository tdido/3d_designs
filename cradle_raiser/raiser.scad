length=38;
width=20;
height=70; 
thickness=3;

difference(){
    cube([length+thickness*2,width+thickness*2,height-thickness*2]);
    translate([thickness,thickness,thickness])
        cube([length,width,height]);
    translate([(length+thickness*2)/2,(width+thickness*2)/2,0])
        cylinder(h=thickness,r=3);
}

for ( i = [10:30:130]) 
    translate([0,width+i,0])
        cube([length,width,10]);